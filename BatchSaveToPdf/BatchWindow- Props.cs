﻿using BatchSaveToPdf.Entity;
using BatchSaveToPdf.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BatchSaveToPdf
{
    public partial class BatchWindow : Window, INotifyPropertyChanged
    {
        public ObservableCollection<FileInformation> FileInfos
        {
            get
            {
                return pack.Files;
            }
            set
            {
                pack.Files = value;
                OnPropertyChanged("FileInfos");
                OnPropertyChanged("ExportReady");
            }
        }

        public bool isFolderPath
        {
            get
            {
                return !string.IsNullOrEmpty(pack.path);
            }
            private set { }
        }

        public string FolderPath
        {
            get
            {
                return pack.path;
            }
            set
            {
                pack.path = value;
                OnPropertyChanged("FolderPath");
                OnPropertyChanged("isFolderPath");

            }

        }

        public bool ExportReady
        {
            get
            {
                return FileInfos != null && FileInfos.Count > 0;
            }
        }

        public bool isAddPdfFolder
        {
            get
            {
                return settings.isCreatePdfFolder;
            }
            set
            {
                settings.isCreatePdfFolder = value;
                SettingsService.save(settings);
                OnPropertyChanged("isAddPdfFolder");
            }
        }

        public bool isAddFormatFolder
        {
            get
            {
                return settings.isCreateFormatFolder;
            }
            set
            {
                settings.isCreateFormatFolder = value;
                SettingsService.save(settings);
                OnPropertyChanged("isAddFormatFolder");
            }
        }

        public bool isAddDate
        {
            get
            {
                //return settings.isAddDate;
                return false;
            }
            set
            {
                //settings.isAddDate = value;
                //SettingsService.save(settings);
                OnPropertyChanged("isAddDate");
            }
        }
        public string defaultFolder
        {
            get
            {
                return settings.defaultFolder;
            }
            set
            {
                settings.defaultFolder = value;
                SettingsService.save(settings);
                OnPropertyChanged("defaultFolder");
            }
        }
    }
}
