﻿using BatchSaveToPdf.Entity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BatchSaveToPdf.Services
{
    class SettingsService
    {
        public static ExportSettings read()
        {
            ExportSettings result = new ExportSettings();
            string settingsPath = BatchWindow.GetPath() + @"\options.xml";
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(ExportSettings));
                using (FileStream fs = new FileStream(settingsPath, FileMode.OpenOrCreate))
                {
                    result = xmlSerializer.Deserialize(fs) as ExportSettings;
                }
            }
            catch (Exception e)
            {
                Debug.Print("*********************     error read XML " + e.Message);
                result = new ExportSettings();
                save(result);
            }
            return result;
        }

 
        public static void save(ExportSettings settings)
        {
            string settingsPath = BatchWindow.GetPath() + @"\options.xml";
            XmlSerializer xmlSerializer = new XmlSerializer(settings.GetType());
            using (FileStream fs = new FileStream(settingsPath, FileMode.Create))
            {
                xmlSerializer.Serialize(fs, settings);
            }
        }


    }
}
