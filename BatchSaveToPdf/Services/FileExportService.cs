﻿using BatchSaveToPdf.Entity;
using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BatchSaveToPdf.Services
{
    class FileExportService
    {
        [DllImport("ole32.dll")]
        private static extern int CreateBindCtx(uint reserved, out IBindCtx ppbc);

        static SldWorks swApp;

        public static void export(ObservableCollection<FileInformation> FileInfos)
        {
            swApp = (SldWorks)GetSwAppFromProcess();
            // (sender as BackgroundWorker).ReportProgress(0, sApp.GetArticleDesignationDoc());
            if (swApp == null)
            {
                startSwAndExport(FileInfos);
            }
            else
            {
                exportCollection(FileInfos);
            }

        }

        private static void exportCollection(ObservableCollection<FileInformation> FileInfos)
        {
            foreach (FileInformation fi in FileInfos)
            {
                openFileAndExport(fi);
            }

        }


        private static void startSwAndExport(ObservableCollection<FileInformation> FileInfos)
        {
            string filePath = FileInfos[0].path;
            var proc = new Process();
            proc.StartInfo.FileName = filePath;
            proc.StartInfo.UseShellExecute = true;
            proc.EnableRaisingEvents = true;
            proc.Exited += (handle_sender, eventArgs) =>
            {
                swApp = (SldWorks)GetSwAppFromProcess();
                BatchWindow.worker.ReportProgress(0, FileInfos[0].name);
                if (FileInfos[0].hasProcess)
                    saveToPdf(swApp.ActiveDoc);
                ObservableCollection<FileInformation> shorten = new ObservableCollection<FileInformation>(FileInfos);
                shorten.RemoveAt(0);
                exportCollection(shorten);
                swApp.ExitApp();
            };
            proc.Start();
        }

        private static void openFileAndExport(FileInformation fileInformation)
        {
            if (fileInformation.hasProcess)
            {
                int err = 0;
                int war = 0;
                BatchWindow.worker.ReportProgress(0, fileInformation.name);
                swApp.OpenDoc6(
                    fileInformation.path,
                    (int)swDocumentTypes_e.swDocDRAWING,
                    (int)swOpenDocOptions_e.swOpenDocOptions_Silent,
                    "",
                    err,
                    war
                    );
                saveToPdf(swApp.ActiveDoc);
            }
        }



        private static void saveToPdf(ModelDoc2 model)
        {
            //if (BatchWindow.settings.isAddDate)
            //{
            //    model = addDate(model);
            //}
            string fullDrawingPath = model.GetPathName();
            string fileName = Path.GetFileNameWithoutExtension(fullDrawingPath);
            DrawingDoc drawing = model as DrawingDoc;
            foreach (string name in drawing.GetSheetNames())
            {
                drawing.ActivateSheet(name);
                Sheet sheet = drawing.GetCurrentSheet();
                object sheetProps = sheet.GetProperties();

                string pdfFileName = fileName + @"_" + sheet.GetName() + ".pdf";
                string pdfFullPath = Path.GetDirectoryName(fullDrawingPath) + @"\";
                if (BatchWindow.settings.isCreatePdfFolder)
                {
                    pdfFullPath += @"PDF\";
                }
                if (BatchWindow.settings.isCreateFormatFolder)
                {
                    double[] sheetPropsAsArray = (double[])sheetProps;
                    pdfFullPath += getFormat((int)sheetPropsAsArray[0], sheetPropsAsArray[5], sheetPropsAsArray[6]);
                    pdfFullPath += @"\";
                }
                if (!Directory.Exists(pdfFullPath))
                {
                    Directory.CreateDirectory(pdfFullPath);
                }
                ExportPdfData data = swApp.GetExportFileData((int)swExportDataFileType_e.swExportPdfData);
                data.ExportAs3D = false;
                data.ViewPdfAfterSaving = false;
                data.SetSheets((int)swExportDataSheetsToExport_e.swExportData_ExportSpecifiedSheets, sheet);

                model.Extension.SaveAs(pdfFullPath + pdfFileName, 0, 0, data, 0, 0);
            }
            swApp.CloseDoc(model.GetTitle());
        }

        private static ModelDoc2 addDate(ModelDoc2 model)
        {
            ModelDocExtension docExt = model.Extension;
            CustomPropertyManager swCustProp = docExt.get_CustomPropertyManager("");
            int succes = swCustProp.Add3(
                "Разработал_Дата",
                (int)swCustomInfoType_e.swCustomInfoText,
                DateTime.Now.ToString("dd.MM.yyyy"),
                (int)swCustomPropertyAddOption_e.swCustomPropertyReplaceValue
                );
            return model;
        }

        private static string addPdfFolder(string path)
        {
            return path;
        }

        private static string addFormatFolder(string path)
        {
            return path;
        }


        static string getFormat(int type, double wigth, double heigth)
        {
            string format = "";

            if (type != 12)
            {
                switch (type)
                {
                    case 6: return "А4";
                    case 7: return "А4";
                    case 8: return "А3";
                    case 9: return "А2";
                    case 10: return "А1";
                    case 11: return "А0";
                }
            }
            else
            {
                switch (heigth * 1000)
                {
                    case 297:
                        format = "А4";
                        break;
                    case 420:
                        format = "А3";
                        break;
                    case 594:
                        format = "А2";
                        break;
                    case 841:
                        format = "А1";
                        break;
                }
            }
            if (wigth * 1000 / getFormatWigth(format) > 1)
                return format + "x" + wigth * 1000 / getFormatWigth(format);
            else
                return format;
        }

        static int getFormatWigth(string key)
        {
            switch (key)
            {
                case "А4": return 210;
                case "А3": return 297;
                case "А2": return 420;
                case "А1": return 594;
            }
            return 0;
        }




        private static ISldWorks GetSwAppFromProcess()
        {
            Process[] procs = Process.GetProcessesByName("SLDWORKS");
            if (procs.Length > 0)
            {
                var myPrc = procs[0];

                var monikerName = "SolidWorks_PID_" + myPrc.Id.ToString();

                IBindCtx context = null;
                IRunningObjectTable rot = null;
                IEnumMoniker monikers = null;

                try
                {
                    CreateBindCtx(0, out context);

                    context.GetRunningObjectTable(out rot);
                    rot.EnumRunning(out monikers);

                    var moniker = new IMoniker[1];

                    while (monikers.Next(1, moniker, IntPtr.Zero) == 0)
                    {
                        var curMoniker = moniker.First();

                        string name = null;

                        if (curMoniker != null)
                        {
                            try
                            {
                                curMoniker.GetDisplayName(context, null, out name);
                            }
                            catch (UnauthorizedAccessException)
                            {
                            }
                        }

                        if (string.Equals(monikerName,
                            name, StringComparison.CurrentCultureIgnoreCase))
                        {
                            object app;
                            rot.GetObject(curMoniker, out app);
                            return app as ISldWorks;
                        }
                    }
                }
                finally
                {
                    if (monikers != null)
                    {
                        Marshal.ReleaseComObject(monikers);
                    }

                    if (rot != null)
                    {
                        Marshal.ReleaseComObject(rot);
                    }

                    if (context != null)
                    {
                        Marshal.ReleaseComObject(context);
                    }
                }
            }
            return null;
        }
    }
}
