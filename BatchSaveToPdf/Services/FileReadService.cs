﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using BatchSaveToPdf.Entity;
using System.IO;
using System.Diagnostics;

namespace BatchSaveToPdf.Services
{
    class FileReadService
    {
        public static ObservableCollection<FileInformation> read(string path)
        {
            ObservableCollection<FileInformation> result = new ObservableCollection<FileInformation>();
            if (Directory.Exists(path))
            {
                IEnumerable<string> query = from f in Directory.GetFiles(path)
                                            where Path.GetExtension(f).ToUpper().Equals(".SLDDRW") && !Path.GetFileName(f).StartsWith("~")
                                            select f;
                foreach (var item in query)
                {
                    result.Add(new FileInformation(item));
                }
            }
            return result;
        }

    }
}
