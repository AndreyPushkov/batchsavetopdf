﻿using BatchSaveToPdf.Entity;
using BatchSaveToPdf.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;

namespace BatchSaveToPdf
{
    public partial class BatchWindow : Window, INotifyPropertyChanged
    {
        public static BackgroundWorker worker;

        public ICommand SelectPathCommand { get; private set; }
        public ICommand ExportCommand { get; private set; }

        public void InitCommands()
        {
            SelectPathCommand = new DelegateCommand((o) => SelectFolder());
            ExportCommand = new DelegateCommand((o) => StartExport());
        }

        private void SelectFolder()
        {
            var dlg = new FolderBrowserDialog();
            dlg.SelectedPath = settings.defaultFolder;
            System.Windows.Forms.DialogResult result = dlg.ShowDialog(this.GetIWin32Window());
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                FolderPath = dlg.SelectedPath;
                FileInfos = FileReadService.read(dlg.SelectedPath);
            }
        }

        private void StartExport()
        {
            worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            pgBar.Visibility = Visibility.Visible;
            worker.RunWorkerAsync();
            
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            FileExportService.export(FileInfos);
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            barStatus.Text = (string)e.UserState;
        }


        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pgBar.Visibility = Visibility.Hidden;
            //System.Windows.MessageBox.Show("Экспорт завершен");
        }
    }
}
