﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatchSaveToPdf.Entity
{
    public class FileInformation
    {
        public FileInformation()
        {
            isDrawing = false;
        }

        public FileInformation(string path)
        {
            this.path = path;
            isDrawing = false;
            this.name = Path.GetFileName(path);
        }

        public string name { get; private set; }
        public string path { get; private set; }
        public bool isDrawing
        {
            get
            {
                return isDrawing;
            }
            private set { }
        }

        public bool isProcessed { get; set; } = false;
        public bool hasProcess { get; set; } = true;
    }
}
