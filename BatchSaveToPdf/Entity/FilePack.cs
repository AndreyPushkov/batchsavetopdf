﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatchSaveToPdf.Entity
{
    public class FilePack
    {
        public string path { get; set; }

        public ObservableCollection<FileInformation> Files { get; set; }

    }
}
