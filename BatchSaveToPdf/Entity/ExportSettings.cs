﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatchSaveToPdf.Entity
{
    [Serializable]
    public class ExportSettings
    {
        public bool isCreatePdfFolder { get; set; } = true;
        public bool isCreateFormatFolder { get; set; } = true;
        //public bool isAddDate { get; set; } = true;
        public string defaultFolder { get; set; } = @"C:\";

    }
}
