﻿using BatchSaveToPdf.Entity;
using BatchSaveToPdf.Services;
using SolidWorks.Interop.sldworks;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BatchSaveToPdf
{
    public partial class BatchWindow : Window, INotifyPropertyChanged
    {
        public static string APP_VERSION = "0.0.1";
        public static string APP_TITLE = "Пакетный экспорт в PDF  ver." + APP_VERSION;

        public static ExportSettings settings;
        public FilePack pack { get; set; } = new FilePack();

        public BatchWindow()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Title = APP_TITLE;
            settings =  SettingsService.read();
            InitCommands();
            //FolderPath = settings.defaultFolder;
            //FileInfos = FileReadService.read(FolderPath);
        }

        public static string GetPath()
        {
            return System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
        }

        #region  INotifyPropertyChanged_IMPL
        //*****************************************
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion
    }
}
